#ifndef UI_CONF_H
#define UI_CONF_H

#include "import/integrators/include/IntegratorType.h"
#include "common.h"

// Windows ids for settings
const QString mainWin   = "mainWindow";
const QString paramWin  = "paramWindow";
const QString inputWin  = "inputWindow";
const QString treeWin   = "treeWindow";
const QString geomWin   = "geoWindow";
const QString expWin    = "expWindow";
const QString strucWin  = "strucWindow";
const QString peaWin    = "peaWindow";
const QString pepWin    = "pepWindow";
const QString pefWin    = "pefWindow";
const QString FMUWin    = "fmuWindow";
const QString perWin    = "perWindow";
const QString pecWin    = "pecWindow";
const QString p2DWin    = "p2DWin";
const QString p3DWin    = "p3DWin";
const QString optWin    = "optWin";

const QString treeExp   = "treeExp";
const QString treeFile  = "treeFile";
const QString treeParam = "treeParam";
const QString treeLog   = "treeLog";
const QString treeStruc = "treeStruc";
const QString treePea   = "treePea";
const QString treePep   = "treePep";
const QString treePef   = "treePef";
const QString treeFMU   = "treeFMU";

const QString tablePer  = "tablePer";
const QString tablePele = "tablePele";


// Titles (actions, groupboxes, labels, buttons and menus)
//
const QString tNewProject    = "New project";
const QString tLoadProject   = "Load project";
const QString tSaveProject   = "Save project";
const QString tSaveProjectAs = "Save project as";
const QString tRecentProjects= "Recent projects";
const QString tExit          = "Exit";
const QString tParameterCal  = "Parameter calibration";
const QString tModel         = "Model";
const QString tProject       = "Project";
const QString tSimulation    = "Simulation";
const QString tModelFile     = "Model file";
const QString tTimeInterval  = "Time interval";
const QString tInputs        = "Inputs";
const QString tOptions       = "Options";
const QString tParameters    = "Parameters";
const QString tProjectName   = "Project name";
const QString tSimulate      = "Simulate";
const QString tValid         = "Valid";
const QString tError         = "Error";
const QString tYes           = "Yes";
const QString tNo            = "No";
const QString tOk            = "Ok";
const QString tFMI20         = "2.0";
const QString tFMI10         = "1.0";
const QString tErrorZipFile  = "The zip file format is not recognized";
const QString tErrorXMlFile  = "The XML file in FMU is missing or not recognized";
const QString tiDate         = "Generation date";
const QString tiTool         = "Generation tool";
const QString tiVersion      = "FMI version:";
const QString tVersion       = "FMI version";
const QString tiInputs       = "Inputs:";
const QString tiParameters   = "Parameters:";
const QString tiOutputs      = "Outputs:";
const QString tiStatus       = "FMU status:";
const QString tiME           = "Model exchange:";
const QString tiCO           = "CoSimulation:";
const QString tME            = "Model exchange";
const QString tCO            = "CoSimulation";
const QString tIntegrator    = "Integrator";
const QString tIntegrator2   = "Integrator:";
const QString tUnit          = "Units";
const QString tDefExp        = "Default exp.:";
const QString tExperiment    = "Experiment";
const QString tExpConf       = "Experiment configuration";
const QString tStartTime     = "Start time:";
const QString tFinalTime     = "Final time:";
const QString tTolerance     = "Tolerance:";
const QString tParamConfig   = "Parameter configuration";
const QString tSimInt        = "Simulation interval:";
const QString tFileTimeVar   = "File and time variable";
const QString tFilename      = "Filename:";
const QString tFilter        = "Filter:";
const QString tTime          = "Time:";
const QString tCaseSensitive = "Case sensitive";
const QString tVariable      = "Variable:";
const QString tVariable2     = "Variable";
const QString tInitialS      = "Initial (s)";
const QString tFinalS        = "Final (s)";
const QString tNIntervals    = "Nº intervals";
const QString tType          = "Type";
const QString tFileType      = "File type:";
const QString tNumVar        = "Nº variables:";
const QString tNumMea        = "Nº samples:";
const QString tTrajectory    = "Trajectory";
const QString tFileStatus    = "File status:";
const QString tModelStruc    = "Model structure";
const QString tStructure     = "Structure";
const QString tAlgorithm     = "Algorithm";
const QString tParEst        = "Parameter Estimation";
const QString tParEstAlg     = "Parameter Estimation - Algorithm";
const QString tInputEstAlg   = "Input Estimation - Algorithm";
const QString tSearchMethod  = "Search method:";
const QString tSearchDomain  = "Search domain:";
const QString tTypeConst     = "Type of constraints:";
const QString tInfoProperty  = "Info property";
const QString tInfoItem      = "Info item";
const QString tProperty      = "Property";
const QString tGradient      = "Gradient";
const QString tFreeDerivative= "Free derivative";
const QString tGlobal        = "Global";
const QString tUnconstraint  = "Unconstrained";
const QString tBoundCons     = "Bound constraints";
const QString tLinear        = "Linear";
const QString tNonlinear     = "Nonlinear";
const QString tGradientReq   = "gradient required";
const QString tGradientNoReq = "no gradient required";
const QString tHessianReq    = "hessian required";
const QString tHessianNoReq  = "no hessian required";
const QString tParEstParams  = "Parameter Estimation - Selected parameters";
const QString tInputEstInputs= "Input Estimation - Selected inputs";
const QString tFMUInfo       = "FMU info";
const QString tScalingType   = "Scaling type";
const QString tScalingValue  = "Scaling value";
const QString tParEstObjFun  = "Parameter Estimation - Objective functions";
const QString tInputEstObj   = "Input Estimation - Objective functions";
const QString tObjFun        = "Objective functions";
const QString tObjective     = "Objective";
const QString tCriteria      = "Criteria";
const QString tCriterion     = "Criterion";
const QString tReduction     = "Reduction";
const QString tWeight        = "Weight";
const QString tFMUinfo       = "FMU info";
const QString tPath          = "Path";
const QString tModelName     = "Model name";
const QString tGuid          = "GUID";
const QString tAuthor        = "Author";
const QString tLicense       = "License";
const QString tCopyright     = "Copyright";
const QString tVariableNaming= "Variable naming";
const QString tNumberOfEvents= "Nº of events";
const QString tExpData       = "Experimental data";
const QString tTypesDef      = "Types definitions";
const QString tParEstResults = "Parameter Estimation - Results";
const QString tParEstConst   = "Parameter Estimation - Constraints";
const QString tInputEstCons  = "Input Estimation - Constraints";
const QString tConstraint    = "Constraint";
const QString tLower         = "Lower";
const QString tUpper         = "Upper";
const QString t2DScatterGrp  = "2D scatter graph";
const QString t3DScatterGrp  = "3D scatter graph";
const QString tInputEst      = "Input estimation";

// Status bar hints
//
const QString sbOpenFMU    = "Open FMU file";
const QString sbRefreshFMU = "Refresh FMU information";
const QString sbDocImport  = "Set initial and final times from file";
const QString sbExpImport  = "Set initial and final tiems from experimental data in the model";
const QString sbError      = "Error";
const QString sbInfo       = "Information";
const QString sbWarning    = "Warning";

// Resources icons
const QString iModelica  = ":/icons/modelica.png";
const QString iFMI       = ":/icons/fmi.png";
const QString iRedDot    = ":/icons/red_dot.png";

// Resource icons (from breeze theme)
//
const QString iNewProject    = ":/icons/document-new";
const QString iSaveProject   = ":/icons/document-save";
const QString iSaveProjectAs = ":/icons/document-save-as";
const QString iExit          = ":/icons/application-exit";
const QString iParameterCal  = ":/icons/archive-insert";
const QString iOptions       = ":/icons/configure-shortcuts";
const QString iModelFile     = ":/icons/document-open";
const QString iSimulate      = ":/icons/media-playback-start";
const QString iStop          = ":/icons/media-playback-stop";
const QString iRefresh       = ":/icons/view-refresh";
const QString iIntegrator2   = ":/icons/labplot-xy-smoothing-curve.svg";
const QString iExperiment    = ":/icons/view-media-playlist";
const QString iApp           = ":/icons/lines-connector";
const QString iField         = ":/icons/view-list-tree";
const QString iDocImp        = ":/icons/document-import";
const QString iStructure     = ":/icons/code-class";
const QString iWarning       = ":/icons/warning.svg";
const QString iInfo          = ":/icons/info.svg";
const QString iError         = ":/icons/error.svg";
const QString iWarning2      = ":/icons/warning2.svg";
const QString iInfo2         = ":/icons/info2.svg";
const QString iError2        = ":/icons/error2.svg";
const QString iOutputs       = ":/icons/timeline-use-zone-on.svg";
const QString iFMU           = ":/icons/fmu.svg";
const QString iObjective     = ":/icons/games-achievements.svg";
const QString iConstraint    = ":/icons/path-outset.svg";
const QString iResults       = ":/icons/office-chart-area.svg";
const QString iMessages      = ":/icons/dialog-messages.svg";
const QString iLog           = ":/icons/view-list-text.svg";
const QString iDocPreview    = ":/icons/document-preview-archive.svg";
const QString iLogOut        = ":/icons/system-log-out.svg";
const QString iPlot          = ":/icons/office-chart-line.svg";
const QString iCalibrate     = ":/icons/labplot-xy-interpolation-curve.svg";
const QString iConstraints   = ":/icons/path-outset.svg";
const QString iCode          = ":/icons/format-text-code.svg";
const QString iAdd           = ":/icons/list-add.svg";
const QString iDelete        = ":/icons/list-remove.svg";
const QString iPlot2         = ":/icons/office-chart-scatter.svg";
//const QString iClipboard     = ":/icons/edit-copy.svg";
const QString iFile          = ":/icons/document-export.svg";
const QString iRun           = ":/icons/system-run.svg";
const QString iRecent        = ":/icons/document-open-recent.svg";
const QString iInputs        = ":/icons/go-next.svg";


// File extensions
const QString eFMU  = "fmu";
const QString eQPF  = "qpf";
//const QString ePDF  = "pdf";
//const QString eSVG  = "svg";
//const QString ePNG  = "png";
//const QString eBMP  = "bmp";
//const QString eJPG  = "jpg";
//const QString eJPEG = "jpeg";

// File filters
const QString ALLFilter  = "All files (*.*)";
const QString FMUFilter  = "FMU file (*."+eFMU+") (*."+eFMU+")";
const QString QPFFilter  = "QPF file (*."+eQPF+") (*."+eQPF+")";
const QString TCFilter   = "All supported input files (*."+eMAT+" *."+eTXT+" *."+eCSV+") (*."+eMAT+" *."+eTXT+" *."+eCSV+")";
const QString TRJFilter  = "Trajectory file (*."+eMAT+" *."+eTXT+") (*."+eMAT+" *."+eTXT+")";
//const QString CSVFilter  = "CSV file (*."+eCSV+") (*."+eCSV+")";
const QString MATFilter  = "Trajectory MAT file (*."+eMAT+") (*."+eMAT+")";
//const QString PDFFilter  = "PDF file (*."+ePDF+") (*."+ePDF+")";
//const QString SVGFilter  = "SVG file (*."+eSVG+") (*."+eSVG+")";
//const QString PNGFilter  = "PNG file (*."+ePNG+") (*."+ePNG+")";
//const QString BMPFilter  = "BMP file (*."+eBMP+") (*."+eBMP+")";
//const QString JPGFilter  = "JPG file (*."+eJPG+") (*."+eJPG+")";
//const QString JPEGFilter = "JPEG file (*."+eJPEG+") (*."+eJPEG+")";

// Message and window titles
const QString mSelectFMU          = "Select FMU file";
const QString mProjectFileOpen    = "Project filename to open";
const QString mSaveProject        = "Do you want to save project changes?";
const QString mProjectSaved       = "Project saved!";
const QString mProjectFileSave    = "Project filename to save";
const QString mCloseConfirmation  = "Close Confirmation";
const QString mExit               = "Are you sure you want to exit?";
const QString mSelectInpFile      = "Select input file";
const QString mInputFile          = "Input file";
const QString mFileNoExist        = "The input file does not exist";
const QString mFileUnsupported    = "The file format is unsupported";
const QString mFileCSVWrongFromat = "Wrong CSV format";
const QString mExperiment         = "Esperiment";
const QString mStartStopTime      = "Stop time must be greater than start time";
const QString mSelectFile         = "Select file";
const QString mResultParEstSave   = "Save optimization results";
const QString mSelectTextEditor   = "Select text editor application";

// Log tree
const unsigned LOG_ICON_SIZE = 24;
const unsigned ST_ICON_SIZE  = 20;

// new line
//const QString newLine = "\n";
//const QString tab     = "\t";

// Status bar time for messages
const double sbTime = 2000;

// Stylesheet collor
const QString clRed   = "color: rgb(255, 0, 0);";
const QString clGreen = "color: rgb(0, 170, 0);";
const QString clBlue  = "color: rgb(0, 0, 127);";
const QString clHigh  = "background-color: rgba(0, 170, 255, 40);";

// Date formats
const QString dfSCREEN1 = "yyyy-MM-dd HH:mm:ss";

// Width trheshold
const int WIDTH_EPS = 10;

// Symbols
const QString sPlus      = "+";
const QString sMinus     = "-";

#endif // UI_CONF_H

