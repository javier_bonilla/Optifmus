#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDialog>

namespace Ui {
class options;
}

class options : public QDialog
{
    Q_OBJECT

public:
    explicit options(QWidget *parent = 0);
    ~options();

private slots:
    void on_bOpen_clicked();
    void on_eFile_textChanged(const QString &arg1);

    void on_buttonBox_accepted();

    void on_bTest_clicked();

private:
    Ui::options *ui;
    void checkEnabled();
};

#endif // OPTIONS_H
