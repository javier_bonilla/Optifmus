#ifndef MODELSTRUCTURE_H
#define MODELSTRUCTURE_H

#include <QDialog>
#include <QTreeWidgetItem>
#include <QKeyEvent>

#include "opt_project.h"

namespace Ui {
class modelStructure;
}

class modelStructure : public QDialog
{
    Q_OBJECT

public:
    explicit modelStructure(QWidget *parent = 0);
    ~modelStructure();
    void populateTree(opt_project prj);
    void keyPressEvent(QKeyEvent *e);
    void saveGeoInfo();

private slots:
    void on_buttonBox_accepted();
    void on_tbClear_clicked();
    void on_lineFilter_returnPressed();

private:
    Ui::modelStructure *ui;
    QList<QWidget *> lw;
};

#endif // MODELSTRUCTURE_H
