#include "fmuinfo.h"
#include "ui_fmuinfo.h"

#include "commonapp.h"
#include "ui_conf.h"

const int COL_NAME  = 0;
const int COL_VALUE = 1;
const int COL_LAST  = 2;

const int WIDTH_NAME  = 150;
const int WIDTH_VALUE = 300;
const int WIDTH_LAST  = 10;


FMUinfo::FMUinfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FMUinfo)
{
    ui->setupUi(this);
    readGeometry(this,FMUWin);

    setWindowTitle(tFMUinfo);
    ui->lTitle->setText(tFMUinfo);

    const QIcon icClear = QIcon(iClear);
    ui->tbClear->setIcon(icClear);

    ui->lbFilter->setText(tFilter);
    ui->cbCaseSentitive->setText(tCaseSensitive);

    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_VALUE,WIDTH_VALUE);
    ui->tree->setColumnWidth(COL_LAST,WIDTH_LAST);

    QStringList headerLabels;

    headerLabels.push_back(tName);
    headerLabels.push_back(tValue);
    headerLabels.push_back(sEmpty);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    ui->lLicense->setStyleSheet(clRed);
    ui->lLicense->setVisible(false);

    readState(ui->tree->header(),treeFMU);
}

void FMUinfo::saveGeoInfo()
{
    writeGeometry(this,FMUWin);
    writeState(ui->tree->header(),treeFMU);
}

FMUinfo::~FMUinfo()
{
    delete ui;
}

void FMUinfo::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

QTreeWidgetItem *newItem(QString name, QString value, QIcon icon = QIcon(iInfo))
{
        QTreeWidgetItem *item = new QTreeWidgetItem();

        item->setIcon(COL_NAME,icon);
        item->setText(COL_NAME,name);
        item->setText(COL_VALUE,value);

        return item;
}

QTreeWidgetItem *listItem(QString title, QList<variable> list, QIcon icon = QIcon(iStruct))
{
    QTreeWidgetItem *item = new QTreeWidgetItem();

    item->setIcon(COL_NAME,icon);
    item->setText(COL_NAME,title);
    for(int i=0;i<list.count();i++)
    {
        QTreeWidgetItem *it = new QTreeWidgetItem();

        it->setText(COL_NAME,list[i].name);
        if (list[i].cau == FMU_PARAMETER)
            it->setText(COL_VALUE,list[i].value);
        switch(list[i].type)
        {
            case FMI_REAL:    it->setIcon(COL_NAME,QIcon(iReal));    break;
            case FMI_INTEGER: it->setIcon(COL_NAME,QIcon(iInteger)); break;
            case FMI_BOOL:    it->setIcon(COL_NAME,QIcon(iBool));    break;
            case FMI_STRING:  it->setIcon(COL_NAME,QIcon(iString));  break;
        }
        item->addChild(it);
    }
    return item;
}

QTreeWidgetItem *expDataItem(opt_model m)
{
    QTreeWidgetItem *item = new QTreeWidgetItem();

    item->setIcon(COL_NAME,QIcon(iExperiment));
    item->setText(COL_NAME,tExpData);

    item->addChild(new QTreeWidgetItem());
    item->child(item->childCount()-1)->setIcon(COL_NAME,QIcon(iInfo));
    item->child(item->childCount()-1)->setText(COL_NAME,tStartTime);
    item->child(item->childCount()-1)->setText(COL_VALUE,QString::number(m.expStart));

    item->addChild(new QTreeWidgetItem());
    item->child(item->childCount()-1)->setIcon(COL_NAME,QIcon(iInfo));
    item->child(item->childCount()-1)->setText(COL_NAME,tFinalTime);
    item->child(item->childCount()-1)->setText(COL_VALUE,QString::number(m.expStop));

    item->addChild(new QTreeWidgetItem());
    item->child(item->childCount()-1)->setIcon(COL_NAME,QIcon(iInfo));
    item->child(item->childCount()-1)->setText(COL_NAME,tStepSize);
    item->child(item->childCount()-1)->setText(COL_VALUE,QString::number(m.expStepSize));

    item->addChild(new QTreeWidgetItem());
    item->child(item->childCount()-1)->setIcon(COL_NAME,QIcon(iInfo));
    item->child(item->childCount()-1)->setText(COL_NAME,tTolerance);
    item->child(item->childCount()-1)->setText(COL_VALUE,QString::number(m.expTolerance));

    return item;
}

QTreeWidgetItem *typeDefItem(opt_model m)
{
    QTreeWidgetItem *item = new QTreeWidgetItem();

    item->setIcon(COL_NAME,QIcon(iExperiment));
    item->setText(COL_NAME,tTypesDef);

    for(int i=0;i<m.types.count();i++)
    {
        QTreeWidgetItem *it = new QTreeWidgetItem();
        QString cad = sEmpty;

        switch(m.types[i].type)
        {
            case FMI_REAL:    it->setIcon(COL_NAME,QIcon(iReal));    break;
            case FMI_INTEGER: it->setIcon(COL_NAME,QIcon(iInteger)); break;
            case FMI_BOOL:    it->setIcon(COL_NAME,QIcon(iBool));    break;
            case FMI_STRING:  it->setIcon(COL_NAME,QIcon(iString));  break;
        }
        it->setText(COL_NAME,m.types[i].name);
        cad = m.types[i].quantity + space;
        if (!m.types[i].unit.isEmpty()) cad = cad +  "[" + m.types[i].unit + "]" + space;
        if (!m.types[i].displayUnit.isEmpty()) cad = cad + "[" + m.types[i].displayUnit + "]";
        it->setText(COL_VALUE,cad);
        item->addChild(it);
    }
    return item;
}

void FMUinfo::populateTree(opt_project prj)
{
    opt_model m = prj.getModel();
    QList<QTreeWidgetItem *> items;

    if (m.valid == FMU_VALID)
    {
        if (!m.modelName.isEmpty()) items.append(newItem(tModelName,m.modelName));
        if (!m.guid.isEmpty()) items.append(newItem(tGuid,m.guid));
        if (!m.vendor.isEmpty())
        {
            QTreeWidgetItem *item = newItem(tiTool,m.vendor);

            if (m.vendor.indexOf(tLicense,0,Qt::CaseInsensitive) >= 0)
            {
                item->setTextColor(1,Qt::red);
                ui->lLicense->setVisible(true);
            }
            items.append(item);
        }
        if (!m.date.toString(dfSCREEN1).isEmpty()) items.append(newItem(tiDate,m.date.toString(dfSCREEN1)));
        if (!m.author.isEmpty()) items.append(newItem(tAuthor,m.author));
        if (!m.desc.isEmpty()) items.append(newItem(tDesc,m.desc));
        if (!m.license.isEmpty()) items.append(newItem(tLicense,m.license));
        if (!m.copyright.isEmpty()) items.append(newItem(tCopyright,m.copyright));
        if (!m.variableNaming.isEmpty()) items.append(newItem(tVariableNaming,m.variableNaming));
        if (!m.numberOfEvents.isEmpty()) items.append(newItem(tNumberOfEvents,m.numberOfEvents));
        items.append(newItem(tME,m.modelExchanged ? tYes : tNo));
        items.append(newItem(tCO,m.cosimulation ? tYes : tNo));
        items.append(newItem(tVersion,m.fmiVer == FMI_2_0 ? tFMI20 : tFMI10));
        if (m.expData) items.append(expDataItem(m));
        if (m.types.count()>0) items.append(typeDefItem(m));
        if (m.params.count()>0) items.append(listItem(tParameters,m.params,QIcon(iParameter)));
        if (m.inputs.count()>0) items.append(listItem(tInputs,m.inputs,QIcon(iInput)));
        if (m.outputs.count()>0) items.append(listItem(tOutputs,m.outputs,QIcon(iOutput)));
        ui->tree->addTopLevelItems(items);
    }
}

void FMUinfo::on_lineFilter_returnPressed()
{
    filterTree(ui->tree,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
}

void FMUinfo::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void FMUinfo::on_buttonBox_accepted()
{
    accept();
}
