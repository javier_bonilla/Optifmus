#ifndef CONFAPP_H
#define CONFAPP_H

// App company, name, version and description
//
const QString company = "CIEMAT-PSA";
const QString appName = "Optifmus";
const QString appDesc = "Optifmus - Optimization of FMUs.";
const QString appVer  = QString::number(VERSION_MAJOR) + "." +
                        QString::number(VERSION_MINOR) + "." +
                        QString::number(VERSION_BUILD);

#endif // CONFAPP_H
