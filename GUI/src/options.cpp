#include "options.h"
#include "ui_options.h"

#include <QFileDialog>
#include <QPushButton>

#include "commonapp.h"
#include "ui_conf.h"

options::options(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::options)
{
    ui->setupUi(this);
    readGeometry(this,optWin);

    ui->eFile->setText(readOption(optTextEditor,defTextEditor));
    ui->checkBox->setChecked(!readOption(optToolBar,defToolBar));

    const QIcon icOpen = QIcon(iLoadProject);
    ui->bOpen->setIcon(icOpen);
    const QIcon icRun = QIcon(iRun);
    ui->bTest->setIcon(icRun);
}

options::~options()
{
    writeGeometry(this,optWin);
    delete ui;
}

void options::on_bOpen_clicked()
{
    QString file = ui->eFile->text().trimmed();

    // Open dialog
    file = QFileDialog::getOpenFileName(this, mSelectTextEditor, file, ALLFilter);
    if (!file.trimmed().isEmpty())
    {
        ui->eFile->setText(file.trimmed());
        checkEnabled();
    }
}

void options::checkEnabled()
{
    // Accept button
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(!ui->eFile->text().trimmed().isEmpty());
    ui->bTest->setEnabled(!ui->eFile->text().trimmed().isEmpty());
}

void options::on_eFile_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    checkEnabled();
}

void options::on_buttonBox_accepted()
{
    writeOption(optTextEditor,ui->eFile->text().trimmed());
    writeOption(optToolBar,ui->checkBox->checkState() == Qt::Unchecked);
}

void options::on_bTest_clicked()
{
    openFileInEditor(sEmpty,ui->eFile->text());
}
