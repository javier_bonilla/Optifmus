#include "parestconstraints.h"
#include "ui_parestconstraints.h"

#include <QPushButton>
#include <memory>

#include "commonapp.h"
#include "ui_conf.h"

const QString ROW           = "ROW";
const QString COL           = "COL";
const QString TABLE         = "TABLE";
const QString LIST_KIND     = "LIST_KIND";
const QString LIST_POS      = "LIST_POS";
const QString LIST_VAR      = "LIST_VAR";
const QString VAR_POS       = "VAR_POS";
const QString LINE_EDIT     = "LINE_EDIT";
const QString SCV           = "SCV";

const QString tInvalidEq = "The constraint is not valid.";

const unsigned LIST_LINEAR_EQ    = 1;
const unsigned LIST_LINEAR_IE    = 2;
const unsigned LIST_NONLINEAR_EQ = 3;
const unsigned LIST_NONLINEAR_IE = 4;

const unsigned VAR_C_A   = 1;
const unsigned VAR_C_B   = 2;
const unsigned VAR_LOWER = 3;
const unsigned VAR_UPPER = 4;
const unsigned VAR_SCT   = 5;
const unsigned VAR_SCV   = 6;

const unsigned SYMBOL_WIDHT = 35;

const QString tMyLineEdit = "MyLineEdit";

// ---------------------------------------------------------------- //
// QLineEdit with focusInEvent reimplemented                        //
// ---------------------------------------------------------------- //
MyLineEdit::MyLineEdit(QWidget *parent):QLineEdit(parent){}

MyLineEdit::~MyLineEdit(){}

void MyLineEdit::focusInEvent(QFocusEvent *e)
{
    QLineEdit::focusInEvent(e);
    QTableWidget *t = (QTableWidget *)this->property(TABLE.toStdString().c_str()).value<void *>();
    t->setCurrentCell(this->property(ROW.toStdString().c_str()).toInt(),
                      this->property(COL.toStdString().c_str()).toInt());
    emit(focussed(this));
}

// ---------------------------------------------------------------- //

parestconstraints::parestconstraints(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parestconstraints)
{
    ui->setupUi(this);
    readGeometry(this,pecWin);

    const QIcon icAdd = QIcon(iAdd);
    const QIcon icDel = QIcon(iDelete);
    ui->bAddLinearEq->setIcon(icAdd);
    ui->bAddNonLinearEq->setIcon(icAdd);
    ui->bAddLinearEq->setIcon(icAdd);
    ui->bAddNonLinearIe->setIcon(icAdd);
    ui->bDeleteLinearEq->setIcon(icDel);
    ui->bDeleteNonLinearEq->setIcon(icDel);
    ui->bDeleteLinearEq->setIcon(icDel);
    ui->bDeleteNonLinearIe->setIcon(icDel);

    readGeometry(ui->tLinearEq,tablePele);
    ui->tabConst->setCurrentIndex(0);

    ui->tLinearEq->setSelectionMode(QAbstractItemView::SingleSelection);

    ui->textLinearEq->clear();
    ui->textLinearIe->clear();
    ui->textNonLinearEq->clear();
    ui->textNonLinearIe->clear();

}

parestconstraints::~parestconstraints()
{
    writeGeometry(this,pecWin);
    writeGeometry(ui->tLinearEq,tablePele);
    delete ui;
}

void parestconstraints::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:  break;
    default: QDialog::keyPressEvent (e);
    }
}

void parestconstraints::on_buttonBox_accepted()
{
    accept();
}

void parestconstraints::on_buttonBox_rejected()
{
    reject();
}

bool parestconstraints::validConstraints()
{
    bool valid = true;

    for(int i=0;i<linear_eq.count() && valid;i++)
        valid = linear_eq[i].valid;

    for(int i=0;i<linear_ie.count() && valid;i++)
        valid = linear_ie[i].valid;

    for(int i=0;i<non_linear_eq.count() && valid;i++)
        valid = non_linear_eq[i].valid;

    for(int i=0;i<non_linear_ie.count() && valid;i++)
        valid = non_linear_ie[i].valid;

    return valid;
}

void parestconstraints::setEnabled()
{
    // Add buttons
    ui->bAddLinearEq->setEnabled(param.count()>0);
    ui->bAddLinearIe->setEnabled(param.count()>0);
    ui->bAddNonLinearEq->setEnabled(output.count()>0);
    ui->bAddNonLinearIe->setEnabled(output.count()>0);
    // Delete buttons
    ui->bDeleteLinearEq->setEnabled(ui->tLinearEq->rowCount()>0 && !ui->tLinearEq->selectedRanges().isEmpty());
    ui->bDeleteLinearIe->setEnabled(ui->tLinearIe->rowCount()>0 && !ui->tLinearIe->selectedRanges().isEmpty());
    ui->bDeleteNonLinearEq->setEnabled(ui->tNonLinearEq->rowCount()>0 && !ui->tNonLinearEq->selectedRanges().isEmpty());
    ui->bDeleteNonLinearIe->setEnabled(ui->tNonLinearIe->rowCount()>0 && !ui->tNonLinearIe->selectedRanges().isEmpty());
    // Equation text
    if (ui->tLinearEq->rowCount()==0)    ui->textLinearEq->clear();
    if (ui->tLinearIe->rowCount()==0)    ui->textLinearIe->clear();
    if (ui->tNonLinearEq->rowCount()==0) ui->textNonLinearEq->clear();
    if (ui->tNonLinearIe->rowCount()==0) ui->textNonLinearIe->clear();

    // Accept button
    QPushButton *button = ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok);
    if (button!=NULL) button->setEnabled(validConstraints());
}

void parestconstraints::on_tLinearEq_clicked(const QModelIndex &index)
{
    Q_UNUSED(index);
    setEnabled();
}

QList<opt_const_ie> parestconstraints::getNon_linear_ie() const
{
    return non_linear_ie;
}

QList<opt_const_eq> parestconstraints::getNon_linear_eq() const
{
    return non_linear_eq;
}

QList<opt_const_ie> parestconstraints::getLinear_ie() const
{
    return linear_ie;
}

QList<opt_const_eq> parestconstraints::getLinear_eq() const
{
    return linear_eq;
}

void parestconstraints::cbSymbol_currentIndexChanged(int index)
{
    if (sender() != NULL)
    {
        // Change value
        QComboBox  *cb = (QComboBox *)sender();
        MyLineEdit *le = (MyLineEdit *)cb->property(LINE_EDIT.toStdString().c_str()).value<void *>();

        unsigned list_kind = le->property(LIST_KIND.toStdString().c_str()).toUInt();
        unsigned list_pos  = le->property(LIST_POS.toStdString().c_str()).toUInt();
        unsigned list_var  = le->property(LIST_VAR.toStdString().c_str()).toUInt();

        le->setEnabled(index>0);

        switch (list_kind)
        {
            case LIST_LINEAR_IE:
                switch(list_var)
                {
                    case VAR_LOWER: linear_ie[list_pos].has_lb = index>0; break;
                    case VAR_UPPER: linear_ie[list_pos].has_ub = index>0; break;
                }
                break;

            case LIST_NONLINEAR_IE:
                switch(list_var)
                {
                    case VAR_LOWER: non_linear_ie[list_pos].has_lb = index>0; break;
                    case VAR_UPPER: non_linear_ie[list_pos].has_ub = index>0; break;
                }
                break;
        }

        // Update equation
        showEquation(cb);

        // Set enabled
        setEnabled();
    }
}

void parestconstraints::cbCons_currentIndexChanged(int index)
{
    if (sender() != NULL)
    {
        // Change value
        QComboBox *cb = (QComboBox *) sender();

        unsigned list_kind = cb->property(LIST_KIND.toStdString().c_str()).toUInt();
        unsigned list_pos  = cb->property(LIST_POS.toStdString().c_str()).toUInt();

        switch (list_kind)
        {
            case LIST_NONLINEAR_EQ:
                for(int i=0;i<non_linear_eq[list_pos].a.count();i++)
                    non_linear_eq[list_pos].a[i] = (i == index ? 1 : 0);
                break;

            case LIST_NONLINEAR_IE:
            for(int i=0;i<non_linear_ie[list_pos].a.count();i++)
                non_linear_ie[list_pos].a[i] = (i == index ? 1 : 0);
            break;
        }

        // Update equation
        showEquation(cb);

        // Set enabled
        setEnabled();
    }
}

void parestconstraints::cbSct_currentIndexChanged(int index)
{
    if (sender() != NULL)
    {
        // Change value
        QComboBox *cb = (QComboBox *) sender();

        unsigned list_kind = cb->property(LIST_KIND.toStdString().c_str()).toUInt();
        unsigned list_pos  = cb->property(LIST_POS.toStdString().c_str()).toUInt();

        switch (list_kind)
        {
            case LIST_LINEAR_EQ:
                linear_eq[list_pos].sct = index;
                break;
            case LIST_LINEAR_IE:
                linear_ie[list_pos].sct = index;
                break;
            case LIST_NONLINEAR_EQ:
                 non_linear_eq[list_pos].sct = index;
                 break;

            case LIST_NONLINEAR_IE:
                non_linear_ie[list_pos].sct = index;
                break;
        }

        // Enable/disable scaling value
        QLineEdit *le = ((QLineEdit *)cb->property(SCV.toStdString().c_str()).value<void *>());
        if (le != NULL) le->setEnabled(index!=scNone && index!=scAuto);
    }

}

void parestconstraints::on_le_textChanged(const QString &s)
{
    // Change value
    if (sender() != NULL)
    {
        MyLineEdit *le = (MyLineEdit *)sender();

        unsigned list_kind = le->property(LIST_KIND.toStdString().c_str()).toUInt();
        unsigned list_pos  = le->property(LIST_POS.toStdString().c_str()).toUInt();
        unsigned list_var  = le->property(LIST_VAR.toStdString().c_str()).toUInt();
        unsigned var_pos   = le->property(VAR_POS.toStdString().c_str()).toUInt();

        switch (list_kind)
        {
            case LIST_LINEAR_EQ:
                switch(list_var)
                {
                    case VAR_C_A: linear_eq[list_pos].a[var_pos] = s.toDouble(); break;
                    case VAR_C_B: linear_eq[list_pos].b          = s.toDouble(); break;
                    case VAR_SCV: linear_eq[list_pos].scv        = s.toDouble(); break;
                }
                break;
            case LIST_LINEAR_IE:
                switch(list_var)
                {
                    case VAR_C_A:   linear_ie[list_pos].a[var_pos] = s.toDouble(); break;
                    case VAR_LOWER: linear_ie[list_pos].lb         = s.toDouble(); break;
                    case VAR_UPPER: linear_ie[list_pos].ub         = s.toDouble(); break;
                    case VAR_SCV:   linear_ie[list_pos].scv        = s.toDouble(); break;
                }
                break;
            case LIST_NONLINEAR_EQ:
                switch(list_var)
                {
                    case VAR_C_B: non_linear_eq[list_pos].b   = s.toDouble(); break;
                    case VAR_SCV: non_linear_eq[list_pos].scv = s.toDouble(); break;
                }
                break;
            case LIST_NONLINEAR_IE:
                switch(list_var)
                {
                    case VAR_LOWER: non_linear_ie[list_pos].lb  = s.toDouble(); break;
                    case VAR_UPPER: non_linear_ie[list_pos].ub  = s.toDouble(); break;
                    case VAR_SCV:   non_linear_ie[list_pos].scv = s.toDouble(); break;
                }
                break;
        }

        // Update equation
        showEquation(le);

        // Set enabled
        setEnabled();
    }
}

void parestconstraints::focussed(MyLineEdit *le)
{
    // Show equation
    showEquation(le);

    // Check enabled widgets
    setEnabled();
}

void parestconstraints::showEquation(QWidget *w)
{
    if (w!=NULL)
    {
        unsigned list_kind = w->property(LIST_KIND.toStdString().c_str()).toUInt();
        unsigned list_pos  = w->property(LIST_POS.toStdString().c_str()).toUInt();
        bool     valid;

        switch (list_kind)
        {
            case LIST_LINEAR_EQ:
            {
                valid = showLinEq(list_pos);
                linear_eq[list_pos].valid = valid;
                break;
            }
            case LIST_LINEAR_IE:
            {
                valid = showLinIe(list_pos);
                linear_ie[list_pos].valid = valid;
                break;
            }
            case LIST_NONLINEAR_EQ:
            {
                valid = showNonLinEq(list_pos);
                non_linear_eq[list_pos].valid = valid;
                break;
            }
            case LIST_NONLINEAR_IE:
            {
                valid = showNonLinIe(list_pos);
                non_linear_ie[list_pos].valid = valid;
                break;
            }
        }
    }
}

bool isPreviousSymbol(QString s)
{
    int i = s.count()-1;

    while(i>=0)
    {
        if (s[i] != ' ')
        {
            return s[i] == sEqual[0]     ||
                   s[i] == sLessEqual[0];
        }
        i--;
    }
    return true;
}

bool EqA(QString &s, QList<double> list, QStringList param)
{
    double sum = 0;

    for(int i=0;i<list.count();i++)
    {
        sum += qAbs(list[i]);
        if (list[i]!=0)
        {
            s = s + (list[i] > 0 ? isPreviousSymbol(s) ? sEmpty : space + sPlus + space : space + sMinus + space);
            s = s + (qAbs(list[i]) == 1 ? param[i] : QString::number(qAbs(list[i])) + QChar(0x00B7) + param[i]);
        }
    }

    return sum!=0;
}

bool parestconstraints::showLinEq(int pos)
{
    QString teq = sEmpty;
    bool valid;

    // A*X and check valid
    valid = EqA(teq,linear_eq[pos].a,param);

    // = B
    teq   = teq + space + sEqual + space + QString::number(linear_eq[pos].b);

    // Set text
    ui->textLinearEq->setText(valid ? teq : tInvalidEq);

    return valid;
}

bool parestconstraints::showLinIe(int pos)
{
    QString teq = sEmpty;
    bool valid;

    // Lower bound <
    teq   = linear_ie[pos].has_lb ? QString::number(linear_ie[pos].lb) + space + sLessEqual + space : teq;

    // A*X and check valid
    valid = EqA(teq,linear_ie[pos].a,param) &&
            ((linear_ie[pos].has_lb  && linear_ie[pos].has_ub && linear_ie[pos].lb < linear_ie[pos].ub) ||
             (linear_ie[pos].has_lb  && !linear_ie[pos].has_ub) ||
             (!linear_ie[pos].has_lb && linear_ie[pos].has_ub));

    // < upper bound
    teq   = linear_ie[pos].has_ub ? teq + space + sLessEqual + space + QString::number(linear_ie[pos].ub) : teq;

    // Set text
    ui->textLinearIe->setText(valid ? teq : tInvalidEq);

    return valid;
}

bool parestconstraints::showNonLinEq(int pos)
{
    int index = findSelItem(non_linear_eq[pos]);

    // Text
    QString teq = index>=0 ? output[index] + space + sEqual + space + QString::number(non_linear_eq[pos].b) : tInvalidEq;

    // Set text
    ui->textNonLinearEq->setText(teq);

    return index>=0;
}

bool parestconstraints::showNonLinIe(int pos)
{
    int     index = findSelItem(non_linear_ie[pos]);
    bool    valid;
    QString teq = sEmpty;

    // Valid constraint
    valid = (index>=0) && ((non_linear_ie[pos].has_lb  && non_linear_ie[pos].has_ub && non_linear_ie[pos].lb < non_linear_ie[pos].ub) ||
                           (non_linear_ie[pos].has_lb  && !non_linear_ie[pos].has_ub) ||
                           (!non_linear_ie[pos].has_lb && non_linear_ie[pos].has_ub));

    // Lower bound <
    teq = non_linear_ie[pos].has_lb ? QString::number(non_linear_ie[pos].lb) + space + sLessEqual + space : teq;

    // Constraint
    teq = teq + (valid ? output[index] : sEmpty);

    // < upper bound
    teq = non_linear_ie[pos].has_ub ? teq + space + sLessEqual + space + QString::number(non_linear_ie[pos].ub) : teq;

    // Set text
    ui->textNonLinearIe->setText(valid ? teq : tInvalidEq);

    return valid;
}

MyLineEdit *parestconstraints::newLineEdit(QTableWidget *t, int row, int col, unsigned list_kind, unsigned list_pos, unsigned list_var, unsigned var_pos, double value, bool focus)
{
    MyLineEdit *le = new MyLineEdit(t);

    // Properties
    le->setProperty(ROW.toStdString().c_str(),row);
    le->setProperty(COL.toStdString().c_str(),col);
    le->setProperty(TABLE.toStdString().c_str(),qVariantFromValue((void *)t));
    le->setProperty(LIST_KIND.toStdString().c_str(),list_kind);
    le->setProperty(LIST_POS.toStdString().c_str(),list_pos);
    le->setProperty(LIST_VAR.toStdString().c_str(),list_var);
    le->setProperty(VAR_POS.toStdString().c_str(),var_pos);

    // Signals
    le->connect(le,SIGNAL(textChanged(const QString &)),this,SLOT(on_le_textChanged(const QString &)));
    le->connect(le,SIGNAL(focussed(MyLineEdit *)),this,SLOT(focussed(MyLineEdit *)));

    // Value and validator
    le->setText(QString::number(value));
    setValidator(le,-MAX_DOUBLE,MAX_DOUBLE);

    // Add to table
    t->setCellWidget(row,col,le);
    if (focus) le->setFocus();

    return le;
}

void parestconstraints::newComboBoxSymbol(QTableWidget *t, MyLineEdit *le, int row, int col, unsigned list_kind, unsigned list_pos, unsigned list_var, bool value, bool focus)
{
    QComboBox *cb = new QComboBox(t);

    // Properties
    cb->setProperty(ROW.toStdString().c_str(),row);
    cb->setProperty(COL.toStdString().c_str(),col);
    cb->setProperty(TABLE.toStdString().c_str(),qVariantFromValue((void *)t));
    cb->setProperty(LIST_KIND.toStdString().c_str(),list_kind);
    cb->setProperty(LIST_POS.toStdString().c_str(),list_pos);
    cb->setProperty(LIST_VAR.toStdString().c_str(),list_var);
    cb->setProperty(LINE_EDIT.toStdString().c_str(),qVariantFromValue((void *)le));

    // Signals
    cb->connect(cb,SIGNAL(currentIndexChanged(int)),this,SLOT(cbSymbol_currentIndexChanged(int)));

    // Combo box items and selected items
    cb->insertItem(0,sEmpty);
    cb->insertItem(1,sLessEqual);
    cb->setCurrentIndex(value ? 1 : 0);

    // Add to table
    t->setCellWidget(row,col,cb);
    if (focus) cb->setFocus();
}

void confSymbolItem(QTableWidgetItem *item)
{
    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
    item->setTextAlignment(Qt::AlignCenter);
}

void parestconstraints::newScaling(QTableWidget *t, int row, int col, unsigned sct, double scv,unsigned list_kind, unsigned list_pos)
{
    QComboBox  *cbsct = new QComboBox(t);
    MyLineEdit *lescv = new MyLineEdit(t);

    // Properties
    cbsct->setProperty(ROW.toStdString().c_str(),row);
    cbsct->setProperty(COL.toStdString().c_str(),col);
    cbsct->setProperty(TABLE.toStdString().c_str(),qVariantFromValue((void *)t));
    cbsct->setProperty(LIST_KIND.toStdString().c_str(),list_kind);
    cbsct->setProperty(LIST_POS.toStdString().c_str(),list_pos);
    cbsct->setProperty(LIST_VAR.toStdString().c_str(),VAR_SCT);
    cbsct->setProperty(SCV.toStdString().c_str(),qVariantFromValue((void *)lescv));


    lescv->setProperty(ROW.toStdString().c_str(),row);
    lescv->setProperty(COL.toStdString().c_str(),col+1);
    lescv->setProperty(TABLE.toStdString().c_str(),qVariantFromValue((void *)t));
    lescv->setProperty(LIST_KIND.toStdString().c_str(),list_kind);
    lescv->setProperty(LIST_POS.toStdString().c_str(),list_pos);
    lescv->setProperty(LIST_VAR.toStdString().c_str(),VAR_SCV);

    // Combo elements
    cbsct->clear();
    cbsct->insertItem(scNone,sctNone);
    cbsct->insertItem(scValue,sctValue);
    cbsct->insertItem(scLog,sctLog);
    cbsct->insertItem(scAuto,sctAuto);

    // Signals
    cbsct->connect(cbsct,SIGNAL(currentIndexChanged(int)),this,SLOT(cbSct_currentIndexChanged(int)));
    lescv->connect(lescv,SIGNAL(textChanged(const QString &)),this,SLOT(on_le_textChanged(const QString &)));

    // Value and validator
    cbsct->setCurrentIndex(sct);
    cbsct->currentIndexChanged(sct);
    lescv->setText(QString::number(scv));
    setValidator(lescv,-MAX_DOUBLE,MAX_DOUBLE);

    // Add to table
    t->setCellWidget(row,col,cbsct);
    t->setCellWidget(row,col+1,lescv);
}

void parestconstraints::newComboBoxCons(QTableWidget *t, int row, int col, unsigned list_kind, unsigned list_pos, unsigned list_var, int value, bool focus)
{
    QComboBox *cb = new QComboBox(t);

    // Properties
    cb->setProperty(ROW.toStdString().c_str(),row);
    cb->setProperty(COL.toStdString().c_str(),col);
    cb->setProperty(TABLE.toStdString().c_str(),qVariantFromValue((void *)t));
    cb->setProperty(LIST_KIND.toStdString().c_str(),list_kind);
    cb->setProperty(LIST_POS.toStdString().c_str(),list_pos);
    cb->setProperty(LIST_VAR.toStdString().c_str(),list_var);

    // Combo box items
    for(int i=0;i<output.count();i++)
        cb->addItem(output[i]);

    // Signals
    cb->connect(cb,SIGNAL(currentIndexChanged(int)),this,SLOT(cbCons_currentIndexChanged(int)));

    cb->setCurrentIndex(value);

    // Add to table
    t->setCellWidget(row,col,cb);
    if (focus) cb->setFocus();
}

void parestconstraints::addEqConsNonLinear(QTableWidget *t, opt_const_eq c, unsigned list_kind, unsigned list_pos)
{
    int nRow = t->rowCount();

    // New line
    t->insertRow(nRow);

    // Insert widget for constraint
    newComboBoxCons(t,nRow,0,list_kind,list_pos,VAR_C_A,findSelItem(c),true);

    // Equal sign
    QTableWidgetItem *item = new QTableWidgetItem(sEqual);

    confSymbolItem(item);
    t->setItem(nRow,1,item);

    // Widget for B value in A*X = B
    newLineEdit(t,nRow,2,list_kind,list_pos,VAR_C_B,0,c.b);

    // Widgets for scaling
    newScaling(t,nRow,3,c.sct,c.scv,list_kind,list_pos);
}

void parestconstraints::addEqConsNonLinear(QTableWidget *t, opt_const_ie c, unsigned list_kind, unsigned list_pos)
{
    int nRow = t->rowCount();

    // New line
    t->insertRow(nRow);

    // Widget for lower bound
    MyLineEdit *le_lb = newLineEdit(t,nRow,0,list_kind,list_pos,VAR_LOWER,0,c.lb,true);

    // Sign <
    newComboBoxSymbol(t,le_lb,nRow,1,list_kind,list_pos,VAR_LOWER,c.has_lb);

    QTableWidgetItem *item1 = new QTableWidgetItem(sLessEqual);
    confSymbolItem(item1);
    t->setItem(nRow,1,item1);

    // Insert widgets for parameters
    newComboBoxCons(t,nRow,2,list_kind,list_pos,VAR_C_A,findSelItem(c));


    // Widget for upper bound
    MyLineEdit *le_ub = newLineEdit(t,nRow,4,list_kind,list_pos,VAR_UPPER,0,c.ub);

    // Sign <
    newComboBoxSymbol(t,le_ub,nRow,3,list_kind,list_pos,VAR_UPPER,c.has_ub);

    // Widgets for scaling
    newScaling(t,nRow,5,c.sct,c.scv,list_kind,list_pos);
}

void parestconstraints::addEqConsLinear(QTableWidget *t, opt_const_eq c, unsigned list_kind, unsigned list_pos)
{
    int nRow = t->rowCount();

    // New line
    t->insertRow(nRow);

    // Insert widgets for parameters
    for(int i=0;i<c.a.count();i++)
        newLineEdit(t,nRow,i,list_kind,list_pos,VAR_C_A,i,c.a[i],i==0 ? true : false);

    // Equal sign
    QTableWidgetItem *item = new QTableWidgetItem(sEqual);

    confSymbolItem(item);
    t->setItem(nRow,c.a.count(),item);

    // Widget for B value in A*X = B
    newLineEdit(t,nRow,c.a.count()+1,list_kind,list_pos,VAR_C_B,0,c.b);

    // Widgets for scaling
    newScaling(t,nRow,c.a.count()+2,c.sct,c.scv,list_kind,list_pos);
}

void parestconstraints::addEqConsLinear(QTableWidget *t, opt_const_ie c, unsigned list_kind, unsigned list_pos)
{
    int nRow = t->rowCount();

    // New line
    t->insertRow(nRow);

    // Widget for lower bound
    MyLineEdit *le_lb = newLineEdit(t,nRow,0,list_kind,list_pos,VAR_LOWER,0,c.lb,true);

    // Sign <
    newComboBoxSymbol(t,le_lb,nRow,1,list_kind,list_pos,VAR_LOWER,c.has_lb);

    QTableWidgetItem *item1 = new QTableWidgetItem(sLessEqual);
    confSymbolItem(item1);
    t->setItem(nRow,1,item1);

    // Insert widgets for parameters
    for(int i=0;i<c.a.count();i++)
        newLineEdit(t,nRow,i+2,list_kind,list_pos,VAR_C_A,i,c.a[i]);

    // Widget for upper bound
    MyLineEdit *le_ub = newLineEdit(t,nRow,c.a.count()+3,list_kind,list_pos,VAR_UPPER,0,c.ub);

    // Sign <
    newComboBoxSymbol(t,le_ub,nRow,c.a.count()+2,list_kind,list_pos,VAR_UPPER,c.has_ub);

    // Widgets for scaling
    newScaling(t,nRow,c.a.count()+4,c.sct,c.scv,list_kind,list_pos);
}

void clearTable(QTableWidget *t)
{
    t->clear();
    while (t->columnCount()>0) t->removeColumn(0);
    while (t->rowCount()>0)    t->removeRow(0);
}

void insertTableNames(QTableWidget *t, QStringList p, int i0 = 0)
{
    for(int i=i0;i<p.count()+i0;i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem();

        item->setText(p[i-i0]);
        t->setHorizontalHeaderItem(i,item);
    }
}

void parestconstraints::configureTableNonLinear(QTableWidget *t, QStringList p, QList<opt_const_eq> l)
{
    clearTable(t);

    if (p.count()>0)
    {
        // Number of columns
        t->setColumnCount(5);

        // Insert columns
        t->setHorizontalHeaderItem(0,new QTableWidgetItem(tConstraint));
        t->setHorizontalHeaderItem(1,new QTableWidgetItem(sEqual));
        t->setHorizontalHeaderItem(2,new QTableWidgetItem(tValue));
        t->setHorizontalHeaderItem(3,new QTableWidgetItem(tScalingType));
        t->setHorizontalHeaderItem(4,new QTableWidgetItem(tScalingValue));
        t->setColumnWidth(1,SYMBOL_WIDHT);

        // Insert data
        for(int i=0;i<l.count();i++)
            addEqConsNonLinear(t,l[i],LIST_NONLINEAR_EQ,i);
    }
}

void parestconstraints::configureTableNonLinear(QTableWidget *t, QStringList p, QList<opt_const_ie> l)
{
    clearTable(t);

    if (p.count()>0)
    {
        // Number of columns
        t->setColumnCount(7);

        // Insert beginning columns
        t->setHorizontalHeaderItem(0,new QTableWidgetItem(tLower));
        t->setHorizontalHeaderItem(1,new QTableWidgetItem(sLessEqual));
        t->setColumnWidth(1,SYMBOL_WIDHT);

        // Insert names
        t->setHorizontalHeaderItem(2,new QTableWidgetItem(tConstraint));

        // Insert end columns
        t->setHorizontalHeaderItem(3,new QTableWidgetItem(sLessEqual));
        t->setHorizontalHeaderItem(4,new QTableWidgetItem(tUpper));
        t->setColumnWidth(3,SYMBOL_WIDHT);
        t->setHorizontalHeaderItem(5,new QTableWidgetItem(tScalingType));
        t->setHorizontalHeaderItem(6,new QTableWidgetItem(tScalingValue));

        // Insert data
        for(int i=0;i<l.count();i++)
            addEqConsNonLinear(t,l[i],LIST_NONLINEAR_IE,i);
    }
}

void parestconstraints::configureTableLinear(QTableWidget *t, QStringList p, QList<opt_const_eq> l)
{
    clearTable(t);

    if (p.count()>0)
    {
        // Number of columns
        t->setColumnCount(p.count()+4);

        // Insert names
        insertTableNames(t,p);

        // Insert additional columns
        t->setHorizontalHeaderItem(p.count(),new QTableWidgetItem(sEqual));
        t->setColumnWidth(p.count(),SYMBOL_WIDHT);
        t->setHorizontalHeaderItem(p.count()+1,new QTableWidgetItem(tValue));
        t->setHorizontalHeaderItem(p.count()+2,new QTableWidgetItem(tScalingType));
        t->setHorizontalHeaderItem(p.count()+3,new QTableWidgetItem(tScalingValue));

        // Insert data
        for(int i=0;i<l.count();i++)
            addEqConsLinear(t,l[i],LIST_LINEAR_EQ,i);
    }
}

void parestconstraints::configureTableLinear(QTableWidget *t, QStringList p, QList<opt_const_ie> l)
{
    clearTable(t);

    if (p.count()>0)
    {
        // Number of columns
        t->setColumnCount(p.count()+6);

        // Insert beginning columns
        t->setHorizontalHeaderItem(0,new QTableWidgetItem(tLower));
        t->setHorizontalHeaderItem(1,new QTableWidgetItem(sLessEqual));
        t->setColumnWidth(1,SYMBOL_WIDHT);

        // Insert names
        insertTableNames(t,p,2);

        // Insert end columns
        t->setHorizontalHeaderItem(p.count()+2,new QTableWidgetItem(sLessEqual));
        t->setColumnWidth(p.count()+2,SYMBOL_WIDHT);
        t->setHorizontalHeaderItem(p.count()+3,new QTableWidgetItem(tUpper));
        t->setHorizontalHeaderItem(p.count()+4,new QTableWidgetItem(tScalingType));
        t->setHorizontalHeaderItem(p.count()+5,new QTableWidgetItem(tScalingValue));

        // Insert data
        for(int i=0;i<l.count();i++)
            addEqConsLinear(t,l[i],LIST_LINEAR_IE,i);
    }
}

void parestconstraints::setData(QList<opt_parameter> p, QList<variable> o, QList<opt_const_eq> leq, QList<opt_const_ie> lie, QList<opt_const_eq> nleq, QList<opt_const_ie> nlie, unsigned type)
{
    ui->lTitle->setText(type == ptInputEst ? tInputEstCons : tParEstConst);
    setWindowTitle(type == ptInputEst ? tInputEst : tParamConfig);

    for(int i=0;i<p.count();i++) param.append(p[i].getDescriptor());
    for(int i=0;i<o.count();i++) output.append(o[i].name);
    linear_eq     = leq;
    linear_ie     = lie;
    non_linear_eq = nleq;
    non_linear_ie = nlie;

    configureTableLinear(ui->tLinearEq,param,linear_eq);
    configureTableLinear(ui->tLinearIe,param,linear_ie);
    configureTableNonLinear(ui->tNonLinearEq,output,non_linear_eq);
    configureTableNonLinear(ui->tNonLinearIe,output,non_linear_ie);
    setEnabled();
}

void parestconstraints::on_bAddLinearEq_clicked()
{
    opt_const_eq c(param.count());

    linear_eq.append(c);
    addEqConsLinear(ui->tLinearEq,c,LIST_LINEAR_EQ,linear_eq.count()-1);
    setEnabled();
}

int deleteSelRow(QTableWidget *t)
{
    int pos = t->selectedRanges()[0].bottomRow();

    if (t->selectedRanges().count()>0)
        t->removeRow(pos);

    // Decrese LIST_POS in sucessive widgets
    for(int i=pos;i<t->rowCount();i++)
    {
        for(int j=0;j<t->columnCount();j++)
        {
            QWidget *w = t->cellWidget(i,j);
            if (w!=NULL)
            {
                int index = w->property(LIST_POS.toStdString().c_str()).toUInt();
                w->setProperty(LIST_POS.toStdString().c_str(),index-1);
            }
        }
    }
    return pos;
}

void parestconstraints::on_bDeleteLinearEq_clicked()
{
    int pos = deleteSelRow(ui->tLinearEq);
    linear_eq.removeAt(pos);
    setEnabled();
}

void parestconstraints::on_bAddLinearIe_clicked()
{
    opt_const_ie c(param.count());

    linear_ie.append(c);
    addEqConsLinear(ui->tLinearIe,c,LIST_LINEAR_IE,linear_ie.count()-1);
    setEnabled();
}

void parestconstraints::on_bDeleteLinearIe_clicked()
{
    int pos = deleteSelRow(ui->tLinearIe);
    linear_ie.removeAt(pos);
    setEnabled();
}

void parestconstraints::on_bAddNonLinearEq_clicked()
{
    opt_const_eq c(output.count());

    if (output.count()>0) c.a[0] = 1; // Default value
    non_linear_eq.append(c);
    addEqConsNonLinear(ui->tNonLinearEq,c,LIST_NONLINEAR_EQ,non_linear_eq.count()-1);
    setEnabled();
}

void parestconstraints::on_bDeleteNonLinearEq_clicked()
{
    int pos = deleteSelRow(ui->tNonLinearEq);
    non_linear_eq.removeAt(pos);
    setEnabled();
}

void parestconstraints::on_bAddNonLinearIe_clicked()
{
    opt_const_ie c(output.count());

    if (output.count()>0) c.a[0] = 1; // Default value
    non_linear_ie.append(c);
    addEqConsNonLinear(ui->tNonLinearIe,c,LIST_NONLINEAR_IE,non_linear_ie.count()-1);
    setEnabled();
}

void parestconstraints::on_bDeleteNonLinearIe_clicked()
{
    int pos = deleteSelRow(ui->tNonLinearIe);
    non_linear_ie.removeAt(pos);
    setEnabled();
}
