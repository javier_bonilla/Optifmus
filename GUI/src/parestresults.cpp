#include "parestresults.h"
#include "ui_parestresults.h"

#include <QClipboard>
#include <QFileDialog>

#include "commonapp.h"
#include "parestplot.h"
#include "parestplot3d.h"
#include "formats/write_matlab4.h"
#include "ui_conf.h"

const QColor clPar  = QColor(190,230,246);
const QColor clObj  = QColor(251,249,215);
const QColor clCons = QColor(250,211,197);

parestresults::parestresults(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::parestresults)
{
    ui->setupUi(this);
    readGeometry(this,perWin);

    const QIcon icClipboard = QIcon(iClipboard);
    const QIcon icFile      = QIcon(iFile);
    const QIcon icPlot      = QIcon(iPlot);
    ui->bClipboard->setIcon(icClipboard);
    ui->bFile->setIcon(icFile);
    ui->bPlot2d->setIcon(icPlot);
    ui->bPlot3d->setIcon(icPlot);

    setWindowTitle(tParEstResults);
    ui->lTitle->setText(tParEstResults);

    readGeometry(ui->table,tablePer);

    ui->table->setSelectionMode(QAbstractItemView::ContiguousSelection);
    ui->table->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->table->setFocus();
    ui->table->setSortingEnabled(true);

    ui->actionCopy_selected_range->setIcon(icClipboard);
    ui->actionExport_to_file->setIcon(icFile);
    cmTable.addAction(ui->actionCopy_selected_range);
    cmTable.addAction(ui->actionExport_to_file);
    on_chbHeader_clicked();
}

parestresults::~parestresults()
{
    writeGeometry(this,perWin);
    writeGeometry(ui->table,tablePer);
    delete ui;
}

void parestresults::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

void parestresults::on_buttonBox_accepted()
{
    accept();
}

void parestresults::setEnabled()
{
    ui->bPlot2d->setEnabled(ui->cb2dx->currentIndex()>=0 &&
                            ui->cb2dy->currentIndex()>=0);
    ui->bPlot3d->setEnabled(ui->cb3dx->currentIndex()>=0 &&
                            ui->cb3dy->currentIndex()>=0 &&
                            ui->cb3dz->currentIndex()>=0);
    ui->bFile->setEnabled(!res.isEmpty());
    ui->bClipboard->setEnabled(!res.isEmpty());
}

QString iconRes(int i, int numPar, int numObj)
{
    return i<numPar ? iOptions : i<numPar+numObj ? iObjective : iConstraint;
}

QColor backgroundColor(int i, int numPar, int numObj)
{
    return i<numPar ? clPar : i<numPar+numObj ? clObj : clCons;
}

void populateCombo(QComboBox *cb, QList<opt_result> list, int numPar, int numObj)
{
    cb->clear();
    for(int i=0;i<list.count();i++)
        cb->insertItem(i,QIcon(iconRes(i,numPar,numObj)),list[i].getName());
}

void populateTable(QTableWidget *table, QList<opt_result> res, int numPar, int numObj)
{
    // Clear table
    table->clear();
    while (table->columnCount()>0) table->removeColumn(0);
    while (table->rowCount()>0)    table->removeRow(0);

    if (res.count()>0)
    {
        // Number of rows and columns
        table->setRowCount(res[0].getValues().count());
        table->setColumnCount(res.count());

        // Insert names
        for(int i=0;i<res.count();i++)
        {
            QTableWidgetItem *item = new QTableWidgetItem();

            item->setIcon(QIcon(iconRes(i,numPar,numObj)));
            item->setText(res[i].getName());
            table->setHorizontalHeaderItem(i,item);
        }

        // Inser data
        for(int i=0;i<res.count();i++)
        {
            for(int j=0;j<res[i].getValues().count();j++)
            {
                QTableWidgetItem *item = new QTableWidgetItem();

                item->setText(QString::number(res[i].getValues()[j]));
                item->setBackgroundColor(backgroundColor(i,numPar,numObj));
                table->setItem(j,i,item);
            }
        }
    }
}

void parestresults::setData(QList<opt_result> p, QList<opt_result> o, QList<opt_result> c)
{
    // Number of parameters and objectives
    numPar = p.count();
    numObj = o.count();

    // Copy parameters
    for(int i=0;i<p.count();i++)
        res.append(p[i]);

    // Copy objectives
    for(int i=0;i<o.count();i++)
        res.append(o[i]);

    // Copy constraints
    for(int i=0;i<c.count();i++)
        res.append(c[i]);

    // Populate combo boxes
    populateCombo(ui->cb2dx,res,numPar,numObj);
    populateCombo(ui->cb2dy,res,numPar,numObj);
    populateCombo(ui->cb3dx,res,numPar,numObj);
    populateCombo(ui->cb3dy,res,numPar,numObj);
    populateCombo(ui->cb3dz,res,numPar,numObj);
    ui->cb2dx->setCurrentIndex(qMin(0,res.count()-1));
    ui->cb2dy->setCurrentIndex(qMin(1,res.count()-1));
    ui->cb3dx->setCurrentIndex(qMin(0,res.count()-1));
    ui->cb3dy->setCurrentIndex(qMin(1,res.count()-1));
    ui->cb3dz->setCurrentIndex(qMin(2,res.count()-1));

    // Populate table
    populateTable(ui->table,res,numPar,numObj);

    // Set enabled
    setEnabled();
}

void parestresults::on_cb2dx_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    setEnabled();
}

void parestresults::on_cb2dy_activated(const QString &arg1)
{
    Q_UNUSED(arg1);
    setEnabled();
}

void parestresults::on_cb3dx_activated(const QString &arg1)
{
    Q_UNUSED(arg1);
    setEnabled();
}

void parestresults::on_cb3dy_activated(const QString &arg1)
{
    Q_UNUSED(arg1);
    setEnabled();
}

void parestresults::on_cb3dz_activated(const QString &arg1)
{
    Q_UNUSED(arg1);
    setEnabled();
}

void parestresults::on_bPlot2d_clicked()
{
    parestplot *pep = new parestplot();
    opt_result  x   = res[ui->cb2dx->currentIndex()];
    opt_result  y   = res[ui->cb2dy->currentIndex()];

    pep->plot2D(x,y);
    pep->exec();
    delete pep;
}

void parestresults::on_bPlot3d_clicked()
{
    parestplot3d *pep = new parestplot3d();
    opt_result  x   = res[ui->cb3dx->currentIndex()];
    opt_result  y   = res[ui->cb3dy->currentIndex()];
    opt_result  z   = res[ui->cb3dz->currentIndex()];

    pep->plot3D(x,y,z);
    pep->exec();
    delete pep;
}

//void copyClipboard(QTableWidget *table, int top, int left, int rowCount, int colCount, bool header)
//{
//    QString str = copy(table,top,left,rowCount,colCount,header);
//    QApplication::clipboard()->setText(str);
//}

void tableToCSVfile(QString filename, QTableWidget *table, int top, int left, int rowCount, int colCount, bool header)
{
    QString str = copy(table,top,left,rowCount,colCount,header,comma);
    QFile   file(filename);

    if(file.open(QIODevice::WriteOnly|QIODevice::Truncate))
    {
        QTextStream stream(&file);
        stream << str;
    }
    file.close();
}

char *QStringListToArrChar(QStringList list, int cols)
{
    int   rows = list.count();
    char  *c   = new char[rows*cols];
    int   pos = 0;

    for(int i=0;i<rows;i++)
    {
        for(int j=0;j<list[i].count();j++)
            c[pos+j*rows] = list[i].toStdString().c_str()[j];
        for(int j=list[i].count();j<cols;j++)
            c[pos+j*rows] = '\0';
        pos++;
    }
    return c;
}

bool tableToMATfile(QString filename, QList<opt_result> res, int top, int left, int rowCount, int colCount, bool addTime = false)
{
    FILE         *fp = NULL;
    QStringList   list;
    char         *array_char;
    int          *array_int;
    double       *array_double;
    int           colIni, colEnd, rowIni, rowEnd, cols, rows, nval, pos;
    opt_result    time;
    QList<double> time_vals;

    rowIni = top;
    rowEnd = rowIni + rowCount;

    colIni = left;
    colEnd = colIni + colCount;

    // Add ficticious time to results
    if (addTime)
    {
        time.setName("time");
        time.setDesc("Simulation time [s]");
        for(int i=0;i<res[0].getValues().count();i++)
            time_vals.append(i);
        time.setValues(time_vals);
        res.insert(colIni,time);
        colEnd += 1; // +1 - Added time variable
    }

    rows = colEnd-colIni;
    nval = rowEnd-rowIni;

    try{
        // open file
        fp = fopen(filename.toStdString().c_str(), "w");
        if (fp==NULL) return false;;

        // write "AClass" matrix
        writeMatVer4AclassNormal(fp);

        // write "name" matrix
        cols = 0;
        for(int i=colIni;i<colEnd;i++)
        {
            QString cad   = res[i].getName();
            int     index = cad.indexOf(sLessEqual);

            if (index>=0) cad = cad.mid(0,index);
            if (cad.count()>cols) cols = cad.count();
            list.append(cad);
        }
        cols++;
        array_char = QStringListToArrChar(list,cols);
        writeMatVer4Matrix(fp,"name", rows, cols, array_char, sizeof(char));
        delete array_char;

        // write "description" matrix
        cols = 0;
        list.clear();
        for(int i=colIni;i<colEnd;i++)
        {
            QString cad   = res[i].getDesc().isEmpty() ? res[i].getName() : res[i].getDesc();
            int     index = cad.indexOf(sLessEqual);

            if (index>=0) cad = cad.mid(0,index);
            if (cad.count()>cols) cols = cad.count();
            list.append(cad);
        }
        cols++;
        array_char = QStringListToArrChar(list,cols);
        writeMatVer4Matrix(fp,"description", rows, cols, array_char, sizeof(char));
        delete array_char;

        // write "dataInfo" matrix
        cols = 4;
        array_int = new int[rows*cols];
        pos = 0;
        for(int i=1;i<=rows;i++)
        {
            array_int[pos+0*rows] =  i==1 ? 0 : 2; /* row 1 - which table */
            array_int[pos+1*rows] =  i;            /* row 2 - index of var in table (variable 'Time' have index 1) */
            array_int[pos+2*rows] =  0;            /* row 3 - linear interpolation == 0 */
            array_int[pos+3*rows] = -1;            /* row 4 - not defined outside of the defined time range == -1 */
            pos++;
        }
        writeMatVer4Matrix(fp,"dataInfo", rows, cols, array_int, sizeof(int));
        delete array_int;

        //  write "data_1" matrix
        array_double = new double [2];
        array_double[0] = addTime ? time_vals.first() : res[0].getValues().first();
        array_double[1] = addTime ? time_vals.last()  : res[0].getValues().last();
        writeMatVer4Matrix(fp,"data_1", 1, 2, array_double, sizeof(double));
        delete array_double;

        //  write "data_2" matrix
        array_double = new double [rows*nval];
        pos = 0;
        for(int i=colIni;i<colEnd;i++)
        {
            QList<double> values = res[i].getValues();
            for(int j=0;j<nval;j++)
                array_double[pos+j] = values[j+rowIni];
            pos+=nval;
        }
        writeMatVer4Matrix(fp,"data_2", nval, rows, array_double, sizeof(double));
        delete array_double;

        fflush(fp);

    } catch(...)
    {
        fclose(fp);
        return false;
    }
    fclose(fp);
    return true;
}

void parestresults::tableToFile(QTableWidget *table, int top, int left, int rowCount, int colCount, bool header)
{
    QFileDialog dlg(this, mResultParEstSave, sEmpty, CSVFilter + ";;" + MATFilter);

    dlg.setAcceptMode(QFileDialog::AcceptSave);
    dlg.setOption(QFileDialog::DontConfirmOverwrite,false);

    if (dlg.exec())
    {
        if (dlg.selectedNameFilter() == CSVFilter)
            tableToCSVfile(dlg.selectedFiles()[0],table,top,left,rowCount,colCount,header);
        else
            tableToMATfile(dlg.selectedFiles()[0],res,top,left,rowCount,colCount,true);
    }
}

void parestresults::on_bClipboard_clicked()
{
    copyClipboard(ui->table,0,0,ui->table->rowCount(),ui->table->colorCount(),ui->chbHeader->checkState() == Qt::Checked);
}

void parestresults::on_table_customContextMenuRequested(const QPoint &pos)
{
    cmTable.exec(ui->table->viewport()->mapToGlobal(pos));
}

void parestresults::on_actionCopy_selected_range_triggered()
{
    if (ui->table->selectedRanges().count()>0)
    {
        QTableWidgetSelectionRange range = ui->table->selectedRanges()[0];
        copyClipboard(ui->table,range.topRow(),range.leftColumn(),range.rowCount(),range.columnCount(),ui->chbHeader->checkState() == Qt::Checked);
    }
}

void parestresults::on_chbHeader_clicked()
{
    QString strCopy = "Copy selected range";
    QString strFile = "Export to file";
    QString strHead = ui->chbHeader->checkState() == Qt::Checked ? "(headers on)" : "(headers off)";

    ui->actionCopy_selected_range->setText(strCopy + space + strHead);
    ui->actionExport_to_file->setText(strFile + space + strHead);
}

void parestresults::on_bFile_clicked()
{
    tableToFile(ui->table,0,0,ui->table->rowCount(),ui->table->columnCount(),ui->chbHeader->checkState() == Qt::Checked);
}

void parestresults::on_actionExport_to_file_triggered()
{
    if (ui->table->selectedRanges().count()>0)
    {
        QTableWidgetSelectionRange range = ui->table->selectedRanges()[0];
        tableToFile(ui->table,range.topRow(),range.leftColumn(),range.rowCount(),range.columnCount(),ui->chbHeader->checkState() == Qt::Checked);
    }
}
