#include "filetree.h"
#include "ui_filetree.h"
#include "commonapp.h"
#include "ui_conf.h"
#include <QPushButton>
#include <QtDebug>

const int COL_NAME    = 0;
const int COL_PARAM   = 1;
const int COL_VALUE   = 2;
const int COL_DESC    = 3;

const int WIDTH_NAME  = 150;
const int WIDTH_PARAM = 75;
const int WIDTH_DESC  = 250;
const int WIDTH_VALUE = 75;

const int VAR_FILE = 0;
const int IS_PARAM = 1;

fileTree::fileTree(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fileTree)
{
    ui->setupUi(this);
    readGeometry(this,treeWin);
    checkEnabled();

    // Tree headers
    QStringList headerLabels;
    headerLabels.push_back(tVariable2);
    headerLabels.push_back(tParam);
    headerLabels.push_back(tValue);
    headerLabels.push_back(tDesc);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    // Configuration
    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_PARAM,WIDTH_PARAM);
    ui->tree->setColumnWidth(COL_DESC,WIDTH_DESC);
    ui->tree->setColumnWidth(COL_VALUE,WIDTH_VALUE);

    readState(ui->tree->header(),treeFile);
}

fileTree::~fileTree()
{
    writeGeometry(this,treeWin);
    writeState(ui->tree->header(),treeFile);
    delete ui;
}

void fileTree::setInfo(QString file, QString type)
{
    ui->lName->setText(file);
    ui->lType->setText(type);
}

void fileTree::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        // qDebug ("Return/enter pressed");
        // Do nothing
        break;

    default:
        QDialog::keyPressEvent (e);
    }
}

void fileTree::on_buttonBox_accepted()
{
    accept();
}

void fileTree::on_buttonBox_rejected()
{
    reject();
}

void fileTree::on_lineFilter_returnPressed()
{
    filterTree(ui->tree,0,ui->lineFilter->text().trimmed(),ui->cbCaseSentitive->checkState() == Qt::Checked);
    checkEnabled();
}

void fileTree::on_tbClear_clicked()
{
    ui->lineFilter->clear();
    on_lineFilter_returnPressed();
}

void fileTree::checkEnabled()
{
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(ui->tree->selectedItems().count()>0 &&
                                                            ui->tree->selectedItems()[0]->data(IS_PARAM,Qt::UserRole).toBool() == false &&
                                                            ui->tree->selectedItems()[0]->childCount()==0);
}

QString fileTree::selectedItem()
{
    if (ui->tree->selectedItems().count()>0)
        return ui->tree->selectedItems()[0]->data(VAR_FILE,Qt::UserRole).toString();
    return sEmpty;
}

void setItemTreeFile(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable var, QString var_selected, int k, QList<QWidget *> &lw)
{
    Q_UNUSED(prj);
    Q_UNUSED(k);
    Q_UNUSED(lw);

    QCheckBox *chk = new QCheckBox(tree);

    // Info
    item->setData(VAR_FILE,Qt::UserRole,var.name);
    item->setText(COL_DESC,var.desc);
    item->setData(IS_PARAM,Qt::UserRole,var.isParam);
    chk->setChecked(var.isParam);
    chk->setAttribute(Qt::WA_TransparentForMouseEvents);
    chk->setFocusPolicy(Qt::NoFocus);
    tree->setItemWidget(item,COL_PARAM,chk);
    if (var.isParam) item->setText(COL_VALUE,var.value);
    // Icon
    if (var.type == FMI_REAL)
        item->setIcon(COL_NAME, QIcon(iReal));
    else if (var.type == FMI_INTEGER)
        item->setIcon(COL_NAME, QIcon(iInteger));
    else if (var.type == FMI_BOOL)
        item->setIcon(COL_NAME, QIcon(iBool));
    else if (var.type == FMI_STRING)
        item->setIcon(COL_NAME, QIcon(iString));
    else if (var.type == FMI_UNDEFINED)
        item->setIcon(COL_NAME, QIcon(iUndefined));
    // Selected itme
    if (var.name == var_selected)
        item->setSelected(true);
}

void fileTree::populateTree(QList<variable> vars, QString var_selected)
{
    opt_project prj(ptNoType);

    populateTreeGeneric(ui->tree,vars,prj,COL_NAME,var_selected,setItemTreeFile,lw);

    ui->tree->setFocus();
    if (ui->tree->selectedItems().count()>0)
        ui->tree->scrollToItem(ui->tree->selectedItems()[0]);

    checkEnabled();
}

bool fileTree::setSelectedItem(QString var_selected)
{
    QTreeWidgetItemIterator it(ui->tree);


    ui->tree->clearSelection();
    while (*it)
    {
        if ((*it)->data(VAR_FILE,Qt::UserRole).toString() == var_selected)
        {
            ui->tree->setFocus();
            (*it)->setSelected(true);
            ui->tree->scrollToItem(*it);
            return true;
        }
        ++it;
    }
    return false;
}

void fileTree::on_tree_clicked(const QModelIndex &index)
{
    Q_UNUSED(index);

    checkEnabled();
}

void fileTree::on_tree_doubleClicked(const QModelIndex &index)
{
    Q_UNUSED(index);

    if (ui->buttonBox->button(QDialogButtonBox::Ok)->isEnabled())
        accept();
}

void fileTree::clearFilter()
{
    on_tbClear_clicked();
}
