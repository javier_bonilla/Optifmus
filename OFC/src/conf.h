#ifndef CONF_H
#define CONF_H

// Context
const QString cntMain = "main";

// Locale
const QString locale_C = "C";

// Return status
const unsigned rtSUCCESS = 0;
const unsigned rtERROR   = 1;

#endif // CONF_H
