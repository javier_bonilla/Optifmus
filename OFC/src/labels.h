#ifndef LABELS_H
#define LABELS_H

#include <QString>

// Words, characters and symbols
const QString t_filename  = "filename";
const QString t_logDakota = "dakota_log_level";
const QString t_d         = "d";
const QString t_openMP    = "openmp";
const QString t_o         = "o";

// Messages
const QString m_execProject            = "Executes the project given by <filename>.";
const QString m_openMP                 = "Provies OpenMP information.";
const QString m_file_not_found_invalid = "File not found or invalid";
const QString m_invalid_project        = "Invalid project file";
const QString m_simFinished            = "Simulation finished";
const QString m_resultSaveIn           = "Results saved in";
const QString m_startingSimulation     = "Starting simulation";
const QString m_logDakota              = "Dakota log level: SILENT, QUIET, NORMAL, VERBOSE and DEBUG. Default value: NORMAL.";

#endif // LABELS_H
