#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QDebug>
#include <iostream>
#include <omp.h>
#include <mpi.h>

#include "confapp.h"
#include "conf.h"
#include "labels.h"
#include "opt_project.h"
#include "of_simulation.h"
#include "parest.h"

//#include "/usr/include/gperftools/profiler.h"

using namespace std;

// ---------------------------------------------------- //
// Helper functions
// ---------------------------------------------------- //
//void     openMPInfo();
QString  broadCastProjectFile(QString prjFile, int rank);
unsigned openProject(QString prjFile, opt_project **prj);
// ---------------------------------------------------- //

int main(int argc, char *argv[])
{
    //ProfilerStart("profiling");

    QCoreApplication a(argc, argv);

    // Variables
    int          rank;
    opt_project *prj      = NULL;
    int          retVal   = rtSUCCESS;
    int          prj_type = ptNoType;
    QString      dll      = DAKOTA_NORMAL;
    QString      prjFile;


    // MPI initialization
    MPI::Init(argc, argv);
    MPI::COMM_WORLD.Set_errhandler(MPI::ERRORS_THROW_EXCEPTIONS);

    // Get processor rank
    rank = MPI::COMM_WORLD.Get_rank();

    // Override locale settings
    setlocale(LC_NUMERIC,locale_C.toStdString().c_str());

    // Register meta types for signal/slots
    qRegisterMetaType<opt_exp>("opt_exp");
    qRegisterMetaType<opt_problem>("opt_problem");

    // Define app name and version
    QCoreApplication::setApplicationName(appName);
    QCoreApplication::setApplicationVersion(appVer);

    // Only root rank
    if (rank == 0)
    {
        // Parser and application description
        QCommandLineParser parser;
        parser.setApplicationDescription(appDesc);

        // WARNING: It cannot be called MPI_Finalize before exiting in these cases!
        // Add help and version options
        parser.addHelpOption();
        parser.addVersionOption();

        // Add project option
        parser.addPositionalArgument(QCoreApplication::translate(cntMain.toStdString().c_str(), t_filename.toStdString().c_str()),
                                     QCoreApplication::translate(cntMain.toStdString().c_str(), m_execProject.toStdString().c_str()));

        // OpenMP option
        //QCommandLineOption clOpenMP(QStringList() << t_o << t_openMP,
        //                            QCoreApplication::translate(cntMain.toStdString().c_str(), m_openMP.toStdString().c_str()));
        //parser.addOption(clOpenMP);

        // Dakota log level option
        QCommandLineOption cloDakotaLogLevelProject(QStringList() << t_d << t_logDakota,
                                                    QCoreApplication::translate(cntMain.toStdString().c_str(), m_logDakota.toStdString().c_str()));
        parser.addOption(cloDakotaLogLevelProject);

        // Process the actual command line arguments given by the user
        parser.process(a);

        // Get Dakota log level
        dll = parser.value(cloDakotaLogLevelProject).toUpper().trimmed();

        // Process open project option
        QStringList args = parser.positionalArguments();
        prjFile = (args.count()>0) ? args[0] : sEmpty;

        // Process openMP
        //if (parser.isSet(clOpenMP)) openMPInfo();
        // No project file then show help
        if (prjFile.isEmpty()) parser.showHelp();
        // Create an open project, if not found show error
        else retVal = openProject(prjFile,&prj);
    }

    if (retVal == rtSUCCESS)
    {
        // Broadcast project file
        prjFile = broadCastProjectFile(prjFile,rank);

        // Create an open project, if not found show error
        if (rank != 0) retVal = openProject(prjFile,&prj);

        // Project type
        prj_type = prj->getType();
    }

    if (retVal == rtSUCCESS)
    {
        // Process project as a function of type
        switch (prj_type)
        {
            // Simulation project
            case ptSimulation:
            {
                // WARNING: No parallel simulation yet
                if (rank == 0)
                {
                    // Simulation class
                    of_simulation sim(prj);
                    // Run simulation
                    if (sim.run()) retVal = rtERROR;
                }
            }
            break;

            // Parameter estimation project
            case ptEstimation:
            {
                // Parameter estimation function
                dll = (rank == 0) ? dll.isEmpty() ? DAKOTA_NORMAL : dll : DAKOTA_NORMAL;
                runParEst(prj,dll);
            }
            break;

            // No defined project - commnad line info
            case ptNoType:
            {
                retVal = rtSUCCESS;
            }
            break;

            // Invalid project
            default:
            {
                if (rank == 0)
                {
                    cout << m_invalid_project.toStdString();
                    retVal = rtERROR;
                }
            }
        }
    }
    //ProfilerStop();

    // Delete project
    delete prj;

    // MPI finalize
    MPI::Finalize();

    return retVal;
}

//void openMPInfo()
//{
//    // OpenMP info
//    cout << endl;
//    cout << appDesc.toStdString() << endl;
//    cout << "OpenMP info" << endl;
//    cout << "-----------" << endl;
//    cout << "Number of core/processors: " << omp_get_num_procs() << endl;
//    cout << "Max. number of threads: "    << omp_get_max_threads() << endl;
//    cout << endl;
//}

QString broadCastProjectFile(QString prjFile, int rank)
{
    // Rank != 0  --> length = 0
    int length = prjFile.count()+1;
    char *fileName;

    // Broadcast length
    MPI_Bcast(&length,1,MPI_INT,0,MPI_COMM_WORLD);

    // Copy file
    fileName = new char[length];
    if (rank == 0) strcpy(fileName,prjFile.toStdString().c_str());

    // Broadcast file
    MPI_Bcast(fileName,length,MPI_CHAR,0,MPI_COMM_WORLD);

    // Project file
    prjFile = fileName;

    // Delete dynamic object
    delete fileName;

    // Return project file
    return prjFile;
}

unsigned openProject(QString prjFile, opt_project **prj)
{
    *prj = new opt_project(ptNoType);
    if (!(*prj)->load_from_file(prjFile))
    {
        cout << m_file_not_found_invalid.toStdString() << endl;
        return rtERROR;
    }
    else
    {
        // WARNING: This should be automatically done when loading model
        // Set appropiate model path for relative FMU filename to project path
        opt_model mo = (*prj)->getModel();
        mo.setFMUfileName(mo.getFMUfileName(),projectPath(*prj));
        (*prj)->setModel(mo);
    }
    return rtSUCCESS;
}

